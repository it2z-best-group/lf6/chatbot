# Chatbot

## TOC

1. [EPK](#epk)
1. [Installation & Running](#installation--running)
   - [Windows](#windows)
   - [Unix](#unix)
1. [Usage](#usage)
1. [Memes](#memes)

## EPK

Die EPK ist auch als Microsoft Visio Datei vorhanden.

```mermaid
%%{init: {"flowchart": {"defaultRenderer": "elk"}} }%%
flowchart
    start{{Chatbot ist gestartet}}
    user1((User))
    user2((User))
    pc[Computer]
    getinput([Eingabe tätigen])
    xor1((XOR))
    xor2((XOR))
    xor3((XOR))
    and1((ᐱ))
    identified{{Fehler ist identifiziert}}
    identify([Fehler identifizieren])
    known{{Eingabe ist bekannt}}
    unknown{{Eingabe ist unbekannt}}
    moreinput([Weitere Eingaben tätigen])
    answer1[Antwort Chatbot]
    answer2[Antwort Chatbot]
    bot((Bot))
    todb([Eingabe in Datenbank eintragen])
    isdb{{Eingabe ist eingetragen}}
    request([Aufforderung nachkommen])
    relay{{Problem ist weitergegeben}}
    solved{{Problem ist behoben}}

    start --> getinput
    user1 --- identify
    user1 --- getinput
    pc --> getinput
    getinput --> xor1
    xor1 --> unknown
    unknown --> and1
    and1 --> identify
    and1 --> todb
    todb --> isdb
    bot --- todb
    identify --> identified
    identified --> getinput
    xor1 --> known
    known --> xor2
    xor2 --> moreinput
    xor2 --> request
    answer1 --> identify
    answer1 --> moreinput
    user2 --- moreinput
    user2 --- request
    answer2 --> request
    request --> xor3
    xor3 --> relay
    xor3 --> solved

    style user1 fill:yellow
    style user2 fill:yellow
    style bot fill:yellow
    style xor1 fill:grey
    style xor2 fill:grey
    style xor3 fill:grey
    style and1 fill:grey
    style start fill:red
    style unknown fill:red
    style identified fill:red
    style known fill:red
    style solved fill:red
    style relay fill:red
    style isdb fill:red
    style todb fill:green
    style getinput fill:green
    style identify fill:green
    style moreinput fill:green
    style request fill:green
    style pc fill:blue
    style answer1 fill:blue
    style answer2 fill:blue
```

## Installation & Running

### windows

```bat
python3 -m venv .venv &&^
.venv\Scripts\activate &&^
pip install -e . &&^
python3 chatbot\bot.py
```

### unix

```sh
python3 -m venv .venv &&\
source .venv/bin/activate &&\
pip install -e . &&\
python3 chatbot/bot.py
```

## Usage

The bot is pretty much self-explaining, since it right away gives you all the necessary options.

## Memes

![](https://public.eulentier.de/chatbotgpt.png)

```mermaid
flowchart LR
    A[Ask ChatGPT to write a chat bot] --> B{Is it good enough?}
    B -- No --> A
    B -- Yes ----> C[You are a programmer now]
```
