import sqlite3


# Funktion wird definiert und ang zunten mit "if" benutzt, damit nicht der gesamte Code beim Test ausgeführt wird
def main():
    # Datenbank zum Speichern der unbekannten Eingaben wird verbunden
    db = "db.sqlite"
    sqlite3.connect(db).execute(
        "CREATE TABLE IF NOT EXISTS fail(id INTEGER PRIMARY KEY AUTOINCREMENT, request TEXT, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)"
    ).close()

    # Die benötigten Dictionaries werden erstellt
    # Dictionaries bestehen immer aus einem "key" (links vom ":") und einer "value" (rechts vom ":")
    # In manchen Fällen beinhalten values weitere dictionaries
    inputs_outlook = {
        "login": (
            "Please make sure that you are typing in your password correctly."
            + "\n"
            + "More often than not will this be the cause of the issue."
            + "\n"
            + "When you have made sure your password is indeed correct please send a mail to support@itbusiness.com including a brief description of the issue and the e-mail associated with the affected account."
            + "\n"
            + "Support will reach out to you shortly.",
            None,
        ),
        "mailflow": (
            "Please send a mail to support@itbusiness.com including a brief description of the issue and the e-mail associated with the affected account."
            + "\n"
            + "Support will reach out to you shortly.",
            None,
        ),
        "other": ("Please call one of our support employees under 0176 12345678.", None),
    }
    inputs_excel = {
        "lost file": (
            "First, please make sure the lost file is not in the recycle bin."
            + "\n"
            + "If the file cannot be located please send a mail to support@itbusiness.com including the exact time that you have last seen the file, its exact name + location and the e-mail associated with your account."
            + "\n"
            + "Support will load the lost file from backup and notfiy you when the file is back.",
            None,
        ),
        "startup": (
            "First, please restart your computer."
            + "\n"
            + "If the issue persists please call one of our support employees under 0176 12345678.",
            None,
        ),
        "other": ("Please call one of our support employees under 0176 12345678.", None),
    }
    inputs_firefox = {
        "open website": (
            "First, please verify that your computer is connected to the internet."
            + "\n"
            + "If it is then please call one of our support employees under 0176 12345678.",
            None,
        ),
        "other": ("Please call one of our support employees under 0176 12345678.", None),
    }
    inputs_hardware = {
        "telephone": (
            "Please send a mail to support@itbusiness.com including a brief description of the issue and the serial number of the affected device."
            + "\n"
            + "Support will reach out to you shortly.",
            None,
        ),
        "printer": (
            "Please send a mail to support@itbusiness.com including a brief description of the issue and the serial number of the affected device."
            + "\n"
            + "If you know how please also include the IP address of the affected device."
            + "\n"
            + "Support will reach out to you shortly.",
            None,
        ),
        "other": ("Please call one of our support employees under 0176 12345678.", None),
    }
    inputs_software = {
        "outlook": ("In which area of the software do you experience the issue?", inputs_outlook),
        "excel": ("In which area of the software do you experience the issue?", inputs_excel),
        "firefox": ("In which area of the software do you experience the issue?", inputs_firefox),
        "other": ("Please call one of our support employees under 0176 12345678.", None),
    }
    inputs_default = {
        "software": ("With which of the firms software do you exprience issues?", inputs_software),
        "hardware": ("What device do you experience issues with?", inputs_hardware),
        "other": ("Please call one of our support employees under 0176 12345678.", None),
    }

    # Deklaration von dem wechselnden dicttionary
    # typehints für bessere autocompletion
    curr_step: dict[str, dict | None] = inputs_default

    # Begrüßungstext mit Anleitung für user
    # '\n' sorgt dafür, dass im Terminal eine neue Zeile begonnen wird, damit es besser aussieht
    # '\n'.join(inputs_default.keys()) sorgt dafür, dass im Begrüßungstext alle Eingabemöglichkeiten aus dem Dictionary "inputs_default" angezeigt werden
    print(
        """
    I am a simple chatbot made by our business to assist in your business related issues.
    I give out answers based on your predefined input.
    If you wish to exit the chat type "exit" (without the "")
    The predefined inputs I can recognize include: \n \n"""
        + "\n".join(curr_step.keys())
        + "\n"
    )
    print("What is your issue related to?")

    # Loop, damit User immer weiter "Inputs" tätigen kann und darauf die entsprechende Antwort bekommt
    while True:
        user_input = input("> ")
        selection = curr_step.get(user_input, None)
        if selection == None:
            if user_input:
                con = sqlite3.connect(db)
                con.execute(f"INSERT INTO fail (request) VALUES ({user_input!r})")
                con.commit()
                con.close()
            continue
        print(selection[0])
        if not selection[1]:
            break
        curr_step = selection[1]
        print("commands: " + ", ".join(curr_step.keys()))

    print("I hope this chat has been of use to you. Have a nice day.")


if __name__ == "__main__":
    main()
