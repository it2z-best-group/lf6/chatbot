from chatbot.bot import main


def test_excel(monkeypatch, capsys):
    values = ["software", "excel", "startup"].__iter__()
    monkeypatch.setattr("builtins.input", lambda _: next(values))
    main()
    captured = capsys.readouterr()
    assert (
        "First, please restart your computer.\nIf the issue persists please call one of our support employees under 0176 12345678.\n"
        in captured.out
    )


def test_printer(monkeypatch, capsys):
    values = ["hardware", "printer"].__iter__()
    monkeypatch.setattr("builtins.input", lambda _: next(values))
    main()
    captured = capsys.readouterr()
    assert (
        "If you know how please also include the IP address of the affected device.\nSupport will reach out to you shortly.\n"
        in captured.out
    )


def test_other(monkeypatch, capsys):
    values = ["other"].__iter__()
    monkeypatch.setattr("builtins.input", lambda _: next(values))
    main()
    captured = capsys.readouterr()
    assert "Please call one of our support employees under 0176 12345678.\n" in captured.out


def test_firefox(monkeypatch, capsys):
    values = ["software", "firefox", "open website"].__iter__()
    monkeypatch.setattr("builtins.input", lambda _: next(values))
    main()
    captured = capsys.readouterr()
    assert "First, please verify that your computer is connected to the internet.\n" in captured.out


def test_not_firefox(monkeypatch, capsys):
    values = ["software", "outlook", "mailflow"].__iter__()
    monkeypatch.setattr("builtins.input", lambda _: next(values))
    main()
    captured = capsys.readouterr()
    assert "First, please verify that your computer is connected to the internet.\n" not in captured.out
